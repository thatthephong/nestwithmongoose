import { Controller, Post, Body, Get } from '@nestjs/common';
import { HongService } from './HongService';
import { CreateHongDto } from './dto/createHongDto';
import { Hong } from './interface/Honginterface';

@Controller('hong')
export class HongController {
  constructor (private readonly hongService:HongService){}

    @Post()
    async create(@Body() CreateHongDto: CreateHongDto){
        await this.hongService.create(CreateHongDto)
    }
    @Get()
    async findAll(): Promise<Hong[]>{
        return this.hongService.findAll()
    }
}


