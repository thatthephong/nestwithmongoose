import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Hong } from './interface/Honginterface';
import { CreateHongDto } from './dto/createHongDto';

@Injectable()
export class HongService {
  constructor(@InjectModel('Hong') private readonly HongModel: Model<Hong>) {}//index0

  async create(createHongDto: CreateHongDto): Promise<Hong> {
    const createdHong = new this.HongModel(createHongDto);
    return await createdHong.save();
  }

  async findAll(): Promise<Hong[]> {
    return await this.HongModel.find().exec();
  }
}