import { Module } from '@nestjs/common';  
import { HongController } from './HongController';
import { HongService } from './HongService';
import { HongSchema } from './schema/HongSchema';
import {MongooseModule} from '@nestjs/mongoose'

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Hong', schema: HongSchema }])],
    controllers: [HongController],
    providers: [ HongService ],
})
export class HongModule { }