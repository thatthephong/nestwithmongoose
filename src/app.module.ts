import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HongModule } from './hong/HongModule';
import {MongooseModule} from '@nestjs/mongoose'

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/nest'),HongModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
